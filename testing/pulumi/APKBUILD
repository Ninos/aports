# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Fraser Waters <frassle@gmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi
pkgver=3.56.0
pkgrel=0
pkgdesc="Infrastructure as Code SDK"
url="https://pulumi.com/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-language-go:_go
	$pkgname-language-nodejs:_nodejs
	$pkgname-language-python:_python
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi/archive/v$pkgver.tar.gz"
# Tests require runtimes for each language provider (python3, go, nodejs, dotnet)
options="!check"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="-X github.com/pulumi/pulumi/pkg/v${pkgver%%.*}/version.Version=v$pkgver"
	mkdir -p "$builddir"/bin

	cd "$builddir"/pkg
	go build -v \
		-o "$builddir"/bin/pulumi \
		-ldflags "$_goldflags" \
		./cmd/pulumi

	cd "$builddir"/sdk
	go build -v \
		-o "$builddir"/bin/pulumi-language-go \
		-ldflags "$_goldflags" \
		./go/pulumi-language-go
	for lang in nodejs python; do
		go build -v \
			-o "$builddir"/bin/pulumi-language-$lang \
			-ldflags "$_goldflags" \
			./$lang/cmd/pulumi-language-$lang
	done

	cd "$builddir"
	./bin/pulumi gen-completion bash > pulumi.bash
	./bin/pulumi gen-completion fish > pulumi.fish
	./bin/pulumi gen-completion zsh > pulumi.zsh
}

package() {
	install -Dm755 bin/pulumi -t "$pkgdir"/usr/bin/

	install -Dm644 pulumi.bash \
		"$pkgdir"/usr/share/bash-completion/completions/pulumi
	install -Dm644 pulumi.fish \
		"$pkgdir"/usr/share/fish/completions/pulumi.fish
	install -Dm644 pulumi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_pulumi
}

_go() {
	pkgdesc="$pkgdesc (Go language provider)"
	depends="$pkgname=$pkgver-r$pkgrel"

	install -Dm755 "$builddir"/bin/pulumi-language-go -t "$subpkgdir"/usr/bin/
}

_nodejs() {
	pkgdesc="$pkgdesc (NodeJS language provider)"
	depends="$pkgname=$pkgver-r$pkgrel"

	install -Dm755 "$builddir"/bin/pulumi-language-nodejs -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/nodejs/dist/pulumi-analyzer-policy \
		-t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/nodejs/dist/pulumi-resource-pulumi-nodejs \
		-t "$subpkgdir"/usr/bin/
}

_python() {
	pkgdesc="$pkgdesc (Python language provider)"
	depends="$pkgname=$pkgver-r$pkgrel python3"

	install -Dm755 "$builddir"/bin/pulumi-language-python -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/cmd/pulumi-language-python-exec -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/dist/pulumi-analyzer-policy-python \
		-t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/dist/pulumi-resource-pulumi-python \
		-t "$subpkgdir"/usr/bin/
}

sha512sums="
92efbe929413c6a2ba015fe1e44adf2d6b53dc3ca562308b2f7d0a3ecff0c3dce7a638f88c5d7cd816129206efa9afa56e62c3b0214a8e0eae64165b88705616  pulumi-3.56.0.tar.gz
"
